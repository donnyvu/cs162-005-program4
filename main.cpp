// Donny Vu
// cs162-005 winter 2024
// 2/26/24
// User would be able to create a game list with information regarding the game. They would be able to display
// all the games created, display matching games wit what the user searched up.

#include "games.h"

int main()
{
	int user_input{0};
	games_list game_collection;

	game_collection.print_welcome();
	user_input = game_collection.get_user_input();

	while(user_input != 5)
	{
		if(user_input == 1)
		{
			game_collection.add_games();
			user_input = game_collection.get_user_input();
		}

		if(user_input == 2)
		{
			game_collection.display_all();
			user_input = game_collection.get_user_input();
		}

		if(user_input == 3)
		{
			game_collection.display_by_genre();
			user_input = game_collection.get_user_input();
		}

		if(user_input == 4)
		{

			game_collection.display_by_platform();
			user_input = game_collection.get_user_input();
		}
		
	}
	if (user_input == 5)
	{
		cout << endl << "Goodbye!" << endl;
	}

}
