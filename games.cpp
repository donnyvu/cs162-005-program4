#include "games.h"

// Donny Vu
// cs162-005 winter 2024
// 2/28/24
// This program will be used to keep track of list of games that the user self creates.
// It can also be used to search through the user created list to display matching members.

// Constructor that initializes variables
games_list::games_list()
{
	cout << "How many games would you like to add to the list? (Whole numbers)." << endl;
	cin >> array_size;
	cin.ignore(100,'\n');

	array = new game_info[array_size];

	num_of_games = 0;
	//name = nullptr;
	search_word = nullptr;
}

// Adds in a new game to the list
void games_list::add_games()
{
	char user_response{'n'};
	char temp[NAME];
	int flag{0};

	cout << endl << "Would you like to add in a game? (y/n): ";
	cin >> user_response;
	cin.ignore(100, '\n');
	user_response = tolower(user_response);

	if(num_of_games == array_size)
	{
		cout << "List is full, user reached previously set number of games in the list." << endl;
	}

	while((user_response == 'y') && (num_of_games < array_size))
	{

		cout << "Please enter the name: ";
		cin.get(temp, NAME, '\n');
		cin.ignore(100,'\n');
		array[num_of_games].name = new char [strlen(temp) + 1];
		strcpy(array[num_of_games].name, temp);

		cout << "Please enter the genre: ";
		cin.get(array[num_of_games].genre, GENRE, '\n');
		cin.ignore(100,'\n');

		cout << "Please enter the numbers of players that can play: ";
		cin >> array[num_of_games].num_of_players;
		cin.ignore(100,'\n');

		cout << "Please enter the review of the game: ";
		cin.get(array[num_of_games].review, REVIEW, '\n');
		cin.ignore(100,'\n');

		cout << "Please enter the price (0.00): $";
		cin >> array[num_of_games].price;
		cin.ignore(100,'\n');

		cout << "Please enter the platform the game runs on: ";
		cin.get(array[num_of_games].platform, PLATFORM, '\n');
		cin.ignore(100,'\n');

		num_of_games = num_of_games + 1;

		cout << endl << "Would you like to add in a game? (y/n): ";
		cin >> user_response;
		cin.ignore(100, '\n');
		user_response = tolower(user_response);
	}
}

// Display all the games in the list
void games_list::display_all()
{	
	cout << "********************Display*All*************************" << endl;
	for(int i{0}; i < num_of_games; ++i)
	{
		cout << "------------------------------------------" << endl;
		cout << "The name: " << array[i].name << endl;
		cout << "The genre: " << array[i].genre << endl;
		cout << "The numbers of players supported: " << array[i].num_of_players << endl;
		cout << "The review: " << array[i].review << endl;
		cout << "The price: " << array[i].price << endl;
		cout << "The platform: " << array[i].platform << endl;
		cout << "------------------------------------------" << endl;
	}
	cout << "*******************************************************" << endl;

}

// Function to display matching genres
void games_list::display_by_genre()
{
	char search_genre[GENRE];

	cout << "Please enter the genre you of the game(s) you would like to display: ";
	cin.get(search_genre, GENRE, '\n');
	cin.ignore(100, '\n');
	search_word = new char [strlen(search_genre) + 1];
	strcpy(search_word, search_genre);

	for(int i{0}; i < num_of_games; ++i)
	{
		if(strcmp(search_word, array[i].genre) == 0)
		{

			cout << "------------------------------------------" << endl;
			cout << "The name: " << array[i].name << endl;
			cout << "The genre: " << array[i].genre << endl;
			cout << "The numbers of players supported: " << array[i].num_of_players << endl;
			cout << "The review: " << array[i].review << endl;
			cout << "The price: " << array[i].price << endl;
			cout << "The platform: " << array[i].platform << endl;
			cout << "------------------------------------------" << endl;
		}
	}
}
// Function to display by platform
void games_list::display_by_platform()
{
	char search_platform[PLATFORM];

	cout << "Please enter the platform you of the game(s) you would like to display: ";
	cin.get(search_platform, PLATFORM, '\n');
	cin.ignore(100, '\n');
	search_word = new char [strlen(search_platform) + 1];
	strcpy(search_word, search_platform);

	for(int i{0}; i < num_of_games; ++i)
	{
		if(strcmp(search_word, array[i].platform) == 0)
		{

			cout << "------------------------------------------" << endl;
			cout << "The name: " << array[i].name << endl;
			cout << "The genre: " << array[i].genre << endl;
			cout << "The numbers of players supported: " << array[i].num_of_players << endl;
			cout << "The review: " << array[i].review << endl;
			cout << "The price: " << array[i].price << endl;
			cout << "The platform: " << array[i].platform << endl;
			cout << "------------------------------------------" << endl;
		}
	}
}

// Deconstructor for the class
games_list::~games_list()
{
	if(array->name != nullptr) // Delete items inside the array first, then the array itself
	{
		delete [] array->name;
	}

	if(array != nullptr)
	{
		delete [] array;
	}

	array->name = nullptr;
	array = nullptr;
	search_word = nullptr; 
	array_size = 0;
	num_of_games = 0;
}

// Prints a nice welcome message
void games_list::print_welcome()
{
	cout << "***********Welcome to my program!**********" << endl;
	cout << "Please select from the menu choices below to begin!" << endl;
	cout << "-------------------------------------------" << endl;
}

// Function to get the user input off the menu choices
int games_list::get_user_input()
{
	int user_input{0};

	cout << "-------------Menu-Choices-------------" << endl;
	cout << "1. Add game" << endl;
	cout << "2. Display all games" << endl;
	cout << "3. Search and display by genre" << endl;
	cout << "4. Search and display games by platform" << endl;
	cout << "5. Quit" << endl;
	cout << "--------------------------------------" << endl;

	cin >> user_input;
	cin.ignore(100, '\n');

	return user_input;
}

