// Donny Vu
// cs162-005 winter 2024
// 2/28/24
// This program will be used to keep track of list of games that the user self creates.
// It can also be used to search through the user created list to display matching members.

#include <iostream>
#include <cctype>
#include <cstring>
using namespace std;

const int NAME{50};
const int GENRE{50};
const int REVIEW{500};
const int PLATFORM{100};

struct game_info
{
	//char name[NAME];
	char *name;
	char genre[GENRE];
	int num_of_players;
	char review[REVIEW];
	float price;
	char platform[PLATFORM];
};

class games_list
{
	public:
		games_list(); // Constructor
		~games_list(); // Deconstructor

		void print_welcome(); // Print welcome message
		int get_user_input(); // Menu choice

		//char y_n_function(); // Y/N function to reduce clutter
		void add_games(); // Add game, loops til user fills up list or quits
		//void add_game(); // Add single game
		void display_all(); // Displays all the games in list
		void display_by_genre(); // WIP
		void display_by_platform(); // WIP
	private:
		game_info * array;

		int num_of_games; // Actual number of how many games there are
		int array_size; // USer set what is the max amount of games in list
		char *search_word; // Used to find a genre and display it
};
